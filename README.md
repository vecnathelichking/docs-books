## Documentation books

CiviCRM documentation is organised into various *books*. The content for a book is written in [markdown](https://docs.civicrm.org/dev/en/master/markdownrules/) and stored in a git repository (for example https://github.com/civicrm/civicrm-user-guide). If a book is translated into different languages, then a separate repository is used for each language. If required, different *versions* of a book can be made by creating different *branches* in a repository. See *defining a new book* below for more information.

## Defining a new book

Books are defined with a Yaml configuration file. To define a new book, create a `.yml` file and add it to the root directory of this repository.

The config file defines the name of the book, the repository that contains the source code, and the **languages** that the book is available in, with a repository for each language. For each language, the configuration file defines:

* which **edition** of the book should be considered **stable**
* where to find the **latest** edits to the book
* a history of book **editions** of the book (these will be publicly listed at https://docs.civicrm.org/).

If you would like to be notified by email whenever an update to a book is published, you can add your email to the notify list.

**Example core book definition:**

``` yml
name: User guide
description: Aimed at day to day users of CiviCRM.
category: core
weight: 10

langs:
  en:
    repo: 'https://github.com/civicrm/civicrm-user-guide'
    latest: master
    stable: 4.7
    history:
      - 4.6
      - 4.5
      - 4.4
    notify: docs@civicrm.org # will be notified when documentation is published (as well as any emails mentioned in commits)
  ca:
    repo: 'https://github.com/babu-cat/civicrm-user-guide-ca'
    latest: master
    # stable: master (will not be published)

icon: user
urls:
  homepage: https://lab.civicrm.org/documentation/docs/user-en
  issues: https://lab.civicrm.org/documentation/docs/user-en/issues
  discuss: https://chat.civicrm.org/documentation

```

**Example extension book definition:**

``` yml
name: CiviVolunteer
description: Create volunteer Projects, manage schedules, sign-up volunteers, and log volunteer hours.
category: extension

langs:
  en:
    repo: 'https://github.com/civicrm/org.civicrm.volunteer'
    versions:
      latest:
        name: Latest
        path: latest
        branch: main

searchterms:
  - civivolunteer
  - volunteer
  - projects
  - schedules
  - sign-up
  - log hours

urls:
  homepage: https://civicrm.org/extensions/volunteer
  issues: https://github.com/civicrm/org.civicrm.volunteer/issues
  discuss: https://chat.civicrm.org/extensions

```

Please make sure you explicitly define the branch name.

## Note for maintainers

Any change to `master` which results in a new book `yml` file or updates an existing book will trigger the pipeline to automatically update the books on the `www-prod` server which hosts `docs.civicrm.org`. 

It is important to understand that this **will not** trigger books to republish/update at this time. Until we have a testing infrastructure setup to validate new book files to ensure they won't break publishing we can't run the publish automatically as it could break things.
